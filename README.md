# Monitoring Infection

Repository containing scripts and data for the paper "Hozé et al., Lancet Public Health, Monitoring the proportion of the population infected by
SARS-CoV-2 using age-stratified hospitalisation and
serological data: a modelling study, 2021" 
